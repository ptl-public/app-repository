# App Repository

Public list of Portal-Apps with configuration and metadata; to be consumed by app-store

## Overview

The Portal app store uses this repository to populate the list of apps that are available for installation.
It uses the state of the master branch by default but can be switched over to the other branches if the user wants to.

See the [Portal developer docs](https://docs.getportal.org/) for more information.
